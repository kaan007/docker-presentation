(function() {
    'use strict';

    angular
        .module('dockerPresentationAppApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
